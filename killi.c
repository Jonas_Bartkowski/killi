#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>

static bool d1 = false;
static bool d2 = false;

int ffsync(FILE* stream)
{
    //fflush writes user buffered data to kernel
    if (!fflush(stream))
        return -1;
    //fsync forces kernel buffered data of a given file descriptor to be written to disk
    //fileno gives us the file descriptor of the stream whose data we just forced to be written to the kernel.
    int fd = fileno(stream);
    if (fd == -1)
        return -2;
    //enforce the write to disk and return the 0 on success or -3 on error.
    int res = fsync(fd);
    if (!res)
        return -3;
    else return res;
}

int main(int argc, char* argv[])
{
    int signal = 15;
    char* string;
    if (argc == 2)
    {
        string = argv[1];
    }
    else if (argc == 3)
    {
        signal = atoi(argv[1]);
        string = argv[2];
    }
    else
    {
        fprintf(stderr, "Please only supply 1 or 2 arguments in the form of 'killi [-signo] string'!\n");
        return -1;
    }

    uid_t uid = getuid();
    if (d2) printf("uid = %d\n", uid);
    char cmnd[20];
    sprintf(cmnd, "/bin/ps -fu%d", uid);

    FILE* stream = popen(cmnd, "r");
    char** line = malloc(sizeof(char *));
    *line = NULL;
    size_t* len = malloc(0);
    ssize_t read;

    while ((read = getline(line, len, stream)) != -1) 
    {
        if (d1) printf("Retrieved line of length %li:\n", read);
        //Q:why is line so big?
        if (d1) printf("%s\n\n", *line);
        //Q: In exercise you say 'To find those output lines of ps that
        //match string', but don't I need to check only the command line
        //parameter?
        char* pids;
        char* cline;
        char delimiter[] = " ";
        char* ptr;
        char* sline = strdup(*line); //this actually fixed the segmentation errors. I have no idea why.
        //char* sline = malloc(sizeof(*line));
        //strcpy(sline, *line);
        ptr = strtok(sline, delimiter);
        if (ptr != NULL)
        {
            int i = 0;
            
            while(ptr != NULL) {
                if (d2) printf("Abschnitt gefunden: %s\n", ptr);
                if (i == 1)
                {
                    pids = ptr;
                }
                else if (i == 7)
                    cline = ptr;
                // nae chsten Abschnitt erstellen
                ptr = strtok(NULL, delimiter);
                i++;
            }
        }
        else if (d1)
            printf("spaces don't delimit this line!\n");

        if (d1) printf("Found pid %s and command line %s!\n", pids, cline);
        //TODO: find out command line content
        
        if (d1) printf("Checking if '%s' is in '%s'!\n", string, cline);
        if (strstr(cline, string) != NULL)
        {
            if (d1) printf("Found target string '%s' in cline!\n", string);
            //TODO: find out pid
            //split 
            int pid;
            if ((pid = atoi(pids)) != getpid())
            {
                //Q: What should be printed out? the whole thing
                printf("%s\n", *line);
                bool done = false;
                while (!done)
                {
                    printf("Send signal '%d'? (y/n)\n", signal);
                    int inp = fgetc(stdin);
                    fgetc(stdin); //remove new line character
                    if (d2) printf("inp = '%c'!\n", inp);
                    if (inp == 'y')
                    {
                        done = true;
                        kill(pid, signal);
                    }
                    else if (inp == 'n')
                    {
                        done = true;
                    }
                }
            }
        }
        //for some reason freeing any o' these leads to crash 
        //aborted: core dumped 'Error in `./killi': free(): invalid pointer:
        /*free(sline);
        free(pids);
        free(cline);
        free(delimiter);*/
    }
    free(*line);
    free(line);
    free(len);
    return 0;
}
